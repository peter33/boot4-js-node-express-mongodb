"use strict";

var mongodb = require('mongodb');
var client = mongodb.MongoClient;

client.connect('mongodb://localhost:27017/cursonode', function(err, db) {
  if (err) {
    return console.log('Error', err);
  }

  db.collection('agentes').find().toArray(function(err, docs) {
    if (err) {
      return console.log('Error', err);
    }

    console.log(docs);

    db.close();

  });

});