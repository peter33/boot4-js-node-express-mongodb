"use strict";

console.log('empiezo');

function escribeTrasDosSegundos(texto, callback) {
  setTimeout(function() {
    console.log(texto);
    callback();
  }, 2000);
}

// los lanzamos en paralelo
for (var i = 0; i < 5; i++) {
  escribeTrasDosSegundos('texto' + i,

  function() {
    console.log('termino');
  }

  );
}
