"use strict";

// constructor de objetos de tipo Coche
function Coche(ruedas) {
  this.ruedas = ruedas;
  this.cuantasRuedas = function() {
    console.log('this', this);
    console.log('tiene', this.ruedas, 'ruedas');
  }
}

var todoterreno = new Coche(4);

var autobus = new Coche(8);

//console.log(todoterreno);

// se pierde el this en el método, y lo preasignamos con .bind(this)
setTimeout(todoterreno.cuantasRuedas.bind(todoterreno), 1000);

//todoterreno.cuantasRuedas();

// llamar a un metodo con un this distinto (prestamo de métodos)
todoterreno.cuantasRuedas.call(autobus);