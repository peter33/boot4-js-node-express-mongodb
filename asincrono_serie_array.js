"use strict";

console.log('empiezo');

function escribeTrasDosSegundos(texto, callback) {
  setTimeout(function() {
    console.log('texto' + texto);
    callback();
  }, 2000);
}

function serie(arr, func, callbackfin) {
  if (arr.length == 0) {
    if (callbackfin) callbackfin();
    return;
  }

  func(arr.shift(), function() {
    serie(arr, func, callbackfin );
  })
}

var elementos = [1,2,3,4,5];

serie(elementos, escribeTrasDosSegundos, function () {
  console.log('terminado');
});



