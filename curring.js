function add(a, b) {
  if (arguments.length === 2) {
    return a + b;
  } else {
    return function(incremento) { return a + incremento }
  }
}

console.log(add(4,5));

var suma5 = add(5);

console.log(suma5(1000));