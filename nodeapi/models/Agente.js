"use strict";

var mongoose = require('mongoose');

// defino el esquema de los agentes
var agenteSchema = mongoose.Schema({
  name: String,
  age: Number
});

agenteSchema.statics.list = function(filter, sort, limit, skip, fields) {
  return new Promise(function(resolve, reject) {
    var query = Agente.find(filter);
    query.sort(sort);
    query.limit(limit);
    query.skip(skip);
    query.select(fields);
    query.exec(function(err, result) {
      if (err) {
        reject(err);
        return;
      }
      resolve(result);
    });
  });
};

var Agente = mongoose.model('Agente', agenteSchema);
