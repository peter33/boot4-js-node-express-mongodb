"use strict";

var express = require('express');
var router = express.Router();

var auth = require('../../lib/authBasico');
var jwtAuth = require('../../lib/jwtAuth');

// basic authentication
//router.use(auth('admin', '12345'));

// json web token auth
router.use(jwtAuth());

router.get('/', function(req, res, next) {
  res.json({ success: true, text: 'zona de admin'});
});

module.exports = router;
