"use strict";

var express = require('express');
var router = express.Router();
var versionModulos = require('../lib/versionModulos');

router.get('/', function(req, res) {
  versionModulos(function(err, modulos) {
    if (err) {
      console.log(err);
      next(err);
      return;
    }

    console.log(modulos);

    res.render('modulos', { modulos: modulos});

  });
});

module.exports = router;