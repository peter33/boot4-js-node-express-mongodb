"use strict";

// hacer paella con promesas

function conArroz( plato) {
  return new Promise(function(resolve, reject) {
    setTimeout(function() {
      resolve(plato + ' arroz');
    }, 500);
  });
}

function conAjo( plato) {
  return new Promise(function(resolve, reject) {
    setTimeout(function() {
      resolve(plato + ' ajo');
      //reject(new Error('no se pudo echar el ajo!'));
    }, 500);
  });
}

function con( plato, ingrediente) {
  return new Promise(function(resolve, reject) {
    setTimeout(function() {
      resolve(plato + ' ' + ingrediente);
    }, 500);
  });
}

var paella = 'paella con';

conArroz(paella)
  .then(conAjo)
  .then(function(plato) {
    return con(plato, 'sal');
  })
  .then(function(plato) {
    console.log(plato);
  })
  .catch(function(err) {
    console.log(err);
  });

