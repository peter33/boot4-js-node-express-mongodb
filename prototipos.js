"use strict";

// definimos un constructor de objetos
function Persona(name) {
  this.name = name;
  this.grita = function () { console.log('GRITO!!!!!!')};
}


// constrimos un objeto
var pepe = new Persona('Pepe');

// añadimos priodades al prototipo de las personas
Persona.prototype.saluda = function() {
  console.log('Hola, me llamo ' + this.name);
};

pepe.saluda();

// Herencia de persona ----------------------

function Agente(name) {
  Persona.call(this, name);
  // heredamos el constructor
  // Esto ejecutará el constructor de Persona sobre el this de Agente // definiendo en el this de Agente sus propiedades.
  // Es como llamar a "super" en otros lenguajes
}

// heredamos las propiedades de las personas
Agente.prototype = new Persona('soy un prototipo!');

var smith = new Agente('Smith');

smith.saluda();
smith.grita();

// Herencia múltiple ---------------------------

function Superheroe() {
  this.vuela = function() {
    console.log( this.name + ' vuela');
  };
  this.esquivaBalas = function() {
    console.log(this.name + ' esquiva balas');
  }
}

Object.assign(Agente.prototype, new Superheroe());

smith.vuela();
smith.esquivaBalas();